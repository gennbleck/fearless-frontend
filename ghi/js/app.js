function createCard(title, description, pictureUrl, eventLocation) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${eventLocation}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
        Stuff
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error ('Bad Response');

        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            console.log(details)
            const description = details.conference.description;
            const eventLocation = details.conference.location.name;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, eventLocation);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error('Error', e);
      // Figure out what to do if an error is raised
    }

  });




  // const data = await response.json();
  //       console.log("data", data)
  //       const conference = data.conferences[0];
  //       console.log(conference)
  //       const nameTag = document.querySelector('.card-title');
  //       nameTag.innerHTML = conference.name;

  //       const descriptionTag = document.querySelector('.card-text');
  //       descriptionTag.innerHTML = conference.description;

  //       const detailUrl = `http://localhost:8000${conference.href}`;
  //       const detailResponse = await fetch(detailUrl);
  //       if (detailResponse.ok) {
  //           const details = await detailResponse.json();
  //           console.log(details)
  //           const imageTag = document.querySelector('.card-img-top');
  //           imageTag.src = details.conference.location.picture_url;

  //       }
